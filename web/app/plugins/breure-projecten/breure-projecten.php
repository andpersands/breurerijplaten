<?php
   /*
   Plugin Name: Breure Projecten
   Plugin URI: http://www.andpersands.com
   Description: a plugin to add a project custom post type
   Version: 1.0
   Author: Andrea Miller
   Author URI: http://www.andpersands.com
   License: GPL2
   */

function breure_project_item() {
	$labels = array(
		'name'               => _x( 'Projecten', 'post type general name' ),
		'singular_name'      => _x( 'Project', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
		'add_new_item'       => __( 'Add New Project item' ),
		'edit_item'          => __( 'Edit Project item' ),
		'new_item'           => __( 'New Project item' ),
		'all_items'          => __( 'All Project items' ),
		'view_item'          => __( 'View Project item' ),
		'search_items'       => __( 'Search Project items' ),
		'not_found'          => __( 'No Project items found' ),
		'not_found_in_trash' => __( 'No Project items found in the Trash' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Projects'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our project items',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => true,
		'menu_icon' 	=> 'dashicons-networking'
	);
	register_post_type( 'project', $args );	
}
add_action( 'init', 'breure_project_item' );

function breure_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['project'] = array(
		0 => '', 
		1 => sprintf( __('Project updated. <a href="%s">View project item</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Project updated.'),
		5 => isset($_GET['revision']) ? sprintf( __('Project restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Project published. <a href="%s">View project</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('Project saved.'),
		8 => sprintf( __('Project submitted. <a target="_blank" href="%s">Preview project item</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview project</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Project draft updated. <a target="_blank" href="%s">Preview project</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'breure_updated_messages' );