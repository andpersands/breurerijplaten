<?php
   /*
   Plugin Name: Breure Services
   Plugin URI: http://www.andpersands.com
   Description: a plugin to add a service custom post type
   Version: 1.0
   Author: Andrea Miller
   Author URI: http://www.andpersands.com
   License: GPL2
   */

function breure_service_item() {
	$labels = array(
		'name'               => _x( 'Werkzaamheden', 'post type general name' ),
		'singular_name'      => _x( 'Service', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
		'add_new_item'       => __( 'Add New Service item' ),
		'edit_item'          => __( 'Edit Service item' ),
		'new_item'           => __( 'New Service item' ),
		'all_items'          => __( 'All Service items' ),
		'view_item'          => __( 'View Service item' ),
		'search_items'       => __( 'Search Service items' ),
		'not_found'          => __( 'No service items found' ),
		'not_found_in_trash' => __( 'No service items found in the Trash' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Services'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our service items',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => true,
		'menu_icon' 	=> 'dashicons-networking'
	);
	register_post_type( 'service', $args );	
}
add_action( 'init', 'breure_service_item' );

function breure_services_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['service'] = array(
		0 => '', 
		1 => sprintf( __('Service updated. <a href="%s">View service item</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Service updated.'),
		5 => isset($_GET['revision']) ? sprintf( __('Service restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Service published. <a href="%s">View service</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('service saved.'),
		8 => sprintf( __('Service submitted. <a target="_blank" href="%s">Preview service item</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Service scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview service</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Service draft updated. <a target="_blank" href="%s">Preview service</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'breure_services_updated_messages' );