<?php
$type = $post->post_name;
if ($type=="contact"){
	get_template_part('templates/content', 'contact_title');
	get_template_part('templates/content', 'page');
} else {
	get_template_part('templates/content', 'title');

	if ($type == "projecten" || $type == "werkzaamheden") {
		if ($type == "projecten"){
			$type = "project";
		get_template_part('templates/content', 'projects');
		} elseif ($type=="werkzaamheden"){
			$type = "service";
			get_template_part('templates/content', 'services');
		}
	}
	else{
		get_template_part('templates/content', 'page');
	}
} ?>