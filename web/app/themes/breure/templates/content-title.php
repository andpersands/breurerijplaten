<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<div class="top-banner" style="background-image: url('<?php echo $image[0]; ?>');">
<?php else: ?>
	<div class="top-banner">
<?php endif; ?>
	<div class="container inner">
		<div class="top-content">
			<div class="top-left-content">
				<div class="page-header">
					<?php $image = get_field('icon');
					if( !empty($image) ): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="pull-left" />
					<?php endif; ?>
					<h1><?php echo roots_title(); ?></h1>
				</div>
			</div>
			<div class="top-right-content">
				<?php the_field('subtitle'); ?>
			</div>
		</div>
	</div>
</div>