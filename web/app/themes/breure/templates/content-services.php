<?php
$args = array(
	'post_type' => 'service',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'order' => 'ASC'
);
$query = null;
$query = new WP_Query($args);
if ($query->have_posts()){ ?>
<div class="container boxes">
	<div class="row">
		<?php while ($query->have_posts()) : $query->the_post(); ?>
		<div class="col-sm-4 col-xs-6 box">
			<a href="<?php echo the_permalink(); ?>" class="clearfix">
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
				<div class="image">
					<div class="overlay"><h4>Bekijk</h4></div>
					<img class="img-responsive" src="<?php echo $image[0]; ?>">
				</div>
			<?php endif; ?>
				<h4><?php the_title(); ?></h4>
			</a>
			<hr />
		</div>
		<?php endwhile; ?>
	</div>
</div>
<?php }
wp_reset_query();?>


