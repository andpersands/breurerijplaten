<div class="top-banner">
	<?php
	$args = array(
	  	'page_id' => 226,
		'post_type' => 'google_maps',
		'post_status' => 'publish',
		'posts_per_page' => 1,
	);
	$query = null;
	$query = new WP_Query($args);
	if ($query->have_posts()){
		while ($query->have_posts()) : $query->the_post();
	    	the_content();
	  	endwhile;
	}
	wp_reset_query(); ?>
</div>