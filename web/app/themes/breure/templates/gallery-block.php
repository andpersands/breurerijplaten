<?php
/**
 * Created by Alex Gurevich.
 * Will create a nice gallery with lightbox uses magnific
 * This plugins requires Advanced Custom Fields (ACF) gallery plugin as well enable gallery function in custom.php
 * Date: 6/12/14
 * Time: 10:14 AM
 */
 //---Enable the scripts and css

    do_action( 'run_gallery' );  // to see what's loaded check /lib/custom.php
?>

<div class="front-gallery">
    <?php $images = get_field('front_gallery'); //<--- set this to your actual field ?>

    <?php if($images): ?>
        <div class="popup-gallery">
            <?php foreach( $images as $image ): ?>
                <a href="<?php echo $image['url']; ?>"
                   class="lightbox-link"
                   title="<?php echo $image['caption']; ?>"
                   data-description="<?php echo $image['description']; ?>">
                    <div class="image-wrap">
                        <img class="img-responsive" src="<?php echo $image['url']; ?>">
                    </div>
                </a>
            <?php endforeach; ?>

            <script type="text/javascript">

                jQuery(document).ready(function($) {
                    $('.popup-gallery').magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        tLoading: 'Loading image #%curr%...',
                        mainClass: 'mfp-img-mobile',
                        gallery: {
                            enabled: true,
                            navigateByImgClick: true,
                            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image: {
                            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                            titleSrc: function(item) {
                                return '<h4 class="lb-title">' + item.el.attr('title') + '</h4>' + '<p class="lb-description">' + item.el.attr('data-description') + '</p>';
                            }
                        }
                    });
                });

            </script>
        </div>

    <?php endif; ?>

</div>