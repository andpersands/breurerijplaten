<?php
/*
    Template Name: Full Page Slider
    based fullPage.js
    each vertical .section is a vertical slide, You can add horizontal slides as well
    https://github.com/alvarotrigo/fullPage.js#fullpagejs
*/
    //---Enable the scripts and css

    do_action( 'run_fullpage' );  // to see what's loaded check /lib/custom.php

?>



<div id="fullpage">
    <div class="section text-center" id="section0">

        <div class="col-md-offset-3 col-lg-6 col-md-6 col-sm-6">

            <?php get_template_part('templates/page', 'header'); ?>
            <?php get_template_part('templates/content', 'page'); ?>

        </div>

    </div>

    <div class="section" id="section1">
        <div class="slide " id="slide1"><h1>Slide Backgrounds</h1></div>
        <div class="slide" id="slide2"><h1>Totally customizable</h1></div>
        <div class="slide" id="slide3"><h1>3rd slide</h1></div>

    </div>


    <div class="section" id="section2">
        <div class="content text-center">
            <div class="intro">
                <h1>No CSS3? No problem!</h1>
                <p>If CSS3 is not available, animations will fall back to jQuery animate.</p>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#fullpage').fullpage({
            slidesColor: ['#1bbc9b', '#4BBFC3', '#7BAABE'],
            css3: true, //try to use css3, if cant do java
            navigation: true, //side dots navigation
            scrollOverflow: true,  //if to much content allow scroll within slide
            resize: false, //resize fonts upon resizing
            paddingTop: '50px',
            paddingBottom: '10px',
            anchors:['Welcome', 'Clients','CaseStudies'] //add nav anchors to slide
        });
    });
</script>