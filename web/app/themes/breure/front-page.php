<?php
/*
Template Name: Front page
*/
?>
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<div class="top-banner" style="background-image: url('<?php echo $image[0]; ?>');">
<?php else: ?>
	<div class="top-banner">
<?php endif; ?>
		<div class="container inner">
			<div class="top-content">
				<div class="top-left-content">
					<div class="page-header">
						<h1><?php echo roots_title(); ?></h1>
					</div>
				</div>
				<div class="top-right-content">
					<?php while (have_posts()) : the_post();
				        the_content();
			     	endwhile; ?>
				</div>
			</div>
			<div class="bottom-content">
				<img src="/app/themes/breure/assets/img/btn-downarrow.png" class="aligncenter">
			</div>
		</div>
	</div>
	<div class="container home-boxes">
		<div class="row">
			<div class="col-sm-4 box werkzaamheden-box">
				<span class="box-heading"><img src="/app/themes/breure/assets/img/icn-huurtarieven.png" class="pull-left"><h5>Huurtarieven</h5></span>
				<?php the_field('huurtarieven_text'); ?>
				<span class="link">&gt; <a href="/tarieven"><?php the_field('huurtarieven_link_text'); ?></a></span>
			</div>
			<div class="col-sm-4 box transport-box">
				<span class="box-heading"><img src="/app/themes/breure/assets/img/icn-transport.png" class="pull-left"><h5>Transport En Advies</h5></span>
				<?php the_field('transport_text'); ?>
				<span class="link">&gt; <a href="/transport"><?php the_field('transport_link_text'); ?></a></span>
			</div>
			<div class="col-sm-4 box offerte-box">
				<span class="box-heading"><img src="/app/themes/breure/assets/img/icn-offerte.png" class="pull-left"><h5>Offerte Aanvragen</h5></span>
				<?php the_field('offerte_text'); ?>
				<span class="link">&gt; <a href="/offerte-aanvargen"><?php the_field('offerte_link_text'); ?></a></span>
			</div>
		</div>
	</div>




